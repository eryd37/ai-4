import copy
import itertools
from typing import List, Dict, Tuple, Callable
import timeit


class CSP:
    def __init__(self):
        # self.variables is a list of the variable names in the CSP
        self.variables: List[str] = []

        # self.domains[i] is a list of legal values for variable i
        self.domains: Dict[str, List[str]] = {}

        # self.constraints[i][j] is a list of legal value pairs for
        # the variable pair (i, j)
        self.constraints: Dict[str, Dict[str, List[Tuple[str, str]]]] = {}

        self.backtrack_calls = 0
        self.backtract_failures = 0

    def add_variable(self, name: str, domain: List[str]):
        """Add a new variable to the CSP. 'name' is the variable name
        and 'domain' is a list of the legal values for the variable.
        """
        self.variables.append(name)
        self.domains[name] = list(domain)
        self.constraints[name] = {}

    def get_all_possible_pairs(self, a: List[str], b: List[str]) -> List[Tuple[str, str]]:
        """Get a list of all possible pairs (as tuples) of the values in
        the lists 'a' and 'b', where the first component comes from list
        'a' and the second component comes from list 'b'.
        """
        return itertools.product(a, b)

    def get_all_arcs(self) -> List[Tuple[str, str]]:
        """Get a list of all arcs/constraints that have been defined in
        the CSP. The arcs/constraints are represented as tuples (i, j),
        indicating a constraint between variable 'i' and 'j'.
        """
        return [(i, j) for i in self.constraints for j in self.constraints[i]]

    def get_all_neighboring_arcs(self, var: str) -> List[Tuple[str, str]]:
        """Get a list of all arcs/constraints going to/from variable
        'var'. The arcs/constraints are represented as in get_all_arcs().
        """
        return [(i, var) for i in self.constraints[var]]

    def add_constraint_one_way(self, i: str, j: str, filter_function: Callable):
        """Add a new constraint between variables 'i' and 'j'. The legal
        values are specified by supplying a function 'filter_function',
        that returns True for legal value pairs and False for illegal
        value pairs. This function only adds the constraint one way,
        from i -> j. You must ensure that the function also gets called
        to add the constraint the other way, j -> i, as all constraints
        are supposed to be two-way connections!
        """
        if not j in self.constraints[i]:
            # First, get a list of all possible pairs of values between variables i and j
            self.constraints[i][j] = self.get_all_possible_pairs(
                self.domains[i], self.domains[j])

        # Next, filter this list of value pairs through the function
        # 'filter_function', so that only the legal value pairs remain
        self.constraints[i][j] = list(
            filter(lambda value_pair: filter_function(*value_pair), self.constraints[i][j]))

    def add_all_different_constraint(self, variables: List[str]):
        """Add an Alldiff constraint between all of the variables in the
        list 'variables'.
        """
        for (i, j) in self.get_all_possible_pairs(variables, variables):
            if i != j:
                self.add_constraint_one_way(i, j, lambda x, y: x != y)

    def backtracking_search(self):
        """This functions starts the CSP solver and returns the found
        solution.
        """
        # Make a so-called "deep copy" of the dictionary containing the
        # domains of the CSP variables. The deep copy is required to
        # ensure that any changes made to 'assignment' does not have any
        # side effects elsewhere.
        assignment = copy.deepcopy(self.domains)

        # Run AC-3 on all constraints in the CSP, to weed out all of the
        # values that are not arc-consistent to begin with
        self.inference(assignment, self.get_all_arcs())

        # Call backtrack with the partial assignment 'assignment'
        return (self.backtrack(assignment), self.backtrack_calls, self.backtract_failures)

    def backtrack(self, assignment: Dict[str, List[str]]):
        """The function 'Backtrack' from the pseudocode in the
        textbook.

        The function is called recursively, with a partial assignment of
        values 'assignment'. 'assignment' is a dictionary that contains
        a list of all legal values for the variables that have *not* yet
        been decided, and a list of only a single value for the
        variables that *have* been decided.

        When all of the variables in 'assignment' have lists of length
        one, i.e. when all variables have been assigned a value, the
        function should return 'assignment'. Otherwise, the search
        should continue. When the function 'inference' is called to run
        the AC-3 algorithm, the lists of legal values in 'assignment'
        should get reduced as AC-3 discovers illegal values.

        IMPORTANT: For every iteration of the for-loop in the
        pseudocode, you need to make a deep copy of 'assignment' into a
        new variable before changing it. Every iteration of the for-loop
        should have a clean slate and not see any traces of the old
        assignments and inferences that took place in previous
        iterations of the loop.
        """
        self.backtrack_calls += 1
        if self.isComplete(assignment):
            return assignment
        var = self.select_unassigned_variable(assignment)
        for value in assignment[var]:
            assignment_copy = copy.deepcopy(assignment)
            assignment_copy[var] = [value]
            if self.inference(assignment_copy, self.get_all_neighboring_arcs(var)):
                result = self.backtrack(assignment_copy)
                if result:
                    return result
        self.backtract_failures += 1
        return False

    def isComplete(self, assignment: Dict[str, List[str]]) -> bool:
        complete = True
        for x in self.variables:
            if len(assignment[x]) != 1:
                complete = False
                break
        return complete

    def select_unassigned_variable(self, assignment: Dict[str, List[str]]) -> str:
        """The function 'Select-Unassigned-Variable' from the pseudocode
        in the textbook. Should return the name of one of the variables
        in 'assignment' that have not yet been decided, i.e. whose list
        of legal values has a length greater than one.
        """
        mrv = self.variables[0]
        for variable in self.variables:
            if len(assignment[variable]) > 1:
                if len(assignment[mrv]) < 2 or len(assignment[variable]) < len(assignment[mrv]):
                    mrv = variable
        return mrv

    def inference(self, assignment: Dict[str, List[int]], queue: List[Tuple[str, str]]) -> bool:
        """The function 'AC-3' from the pseudocode in the textbook.
        'assignment' is the current partial assignment, that contains
        the lists of legal values for each undecided variable. 'queue'
        is the initial queue of arcs that should be visited.
        """
        # Is the order of (x_k, x_i) important?
        # x_k = (x_i, x_k) here
        while len(queue) > 0:
            x = queue.pop(0)
            if self.revise(assignment, x[0], x[1]):
                if len(assignment[x[0]]) == 0:
                    return False
                for x_k in self.get_all_neighboring_arcs(x[0]):
                    if x_k != x:
                        queue.append(x_k)
        return True

    def revise(self, assignment: Dict[str, List[str]], i: str, j: str) -> bool:
        """The function 'Revise' from the pseudocode in the textbook.
        'assignment' is the current partial assignment, that contains
        the lists of legal values for each undecided variable. 'i' and
        'j' specifies the arc that should be visited. If a value is
        found in variable i's domain that doesn't satisfy the constraint
        between i and j, the value should be deleted from i's list of
        legal values in 'assignment'.
        """
        revised = False
        for x in assignment[i]:
            none_possible = True
            for y in assignment[j]:
                if (x, y) in self.constraints[i][j]:
                    none_possible = False
                    break
            if none_possible:
                assignment[i].remove(x)
                revised = True
        return revised


def create_map_coloring_csp() -> CSP:
    """Instantiate a CSP representing the map coloring problem from the
    textbook. This can be useful for testing your CSP solver as you
    develop your code.
    """
    csp = CSP()
    states = ['WA', 'NT', 'Q', 'NSW', 'V', 'SA', 'T']
    edges = {'SA': ['WA', 'NT', 'Q', 'NSW', 'V'],
             'NT': ['WA', 'Q'], 'NSW': ['Q', 'V']}
    colors = ['red', 'green', 'blue']
    for state in states:
        csp.add_variable(state, colors)
    for state, other_states in edges.items():
        for other_state in other_states:
            csp.add_constraint_one_way(state, other_state, lambda i, j: i != j)
            csp.add_constraint_one_way(other_state, state, lambda i, j: i != j)
    return csp


def create_sudoku_csp(filename: str) -> CSP:
    """Instantiate a CSP representing the Sudoku board found in the text
    file named 'filename' in the current directory.
    """
    csp = CSP()
    board = list(map(lambda x: x.strip(), open(filename, 'r')))

    for row in range(9):
        for col in range(9):
            if board[row][col] == '0':
                csp.add_variable('%d-%d' % (row, col),
                                 list(map(str, range(1, 10))))
            else:
                csp.add_variable('%d-%d' % (row, col), [board[row][col]])

    for row in range(9):
        csp.add_all_different_constraint(
            ['%d-%d' % (row, col) for col in range(9)])
    for col in range(9):
        csp.add_all_different_constraint(
            ['%d-%d' % (row, col) for row in range(9)])
    for box_row in range(3):
        for box_col in range(3):
            cells = []
            for row in range(box_row * 3, (box_row + 1) * 3):
                for col in range(box_col * 3, (box_col + 1) * 3):
                    cells.append('%d-%d' % (row, col))
            csp.add_all_different_constraint(cells)

    return csp


def print_sudoku_solution(solution: Dict[str, List[str]]) -> None:
    """Convert the representation of a Sudoku solution as returned from
    the method CSP.backtracking_search(), into a human readable
    representation.
    """
    for row in range(9):
        for col in range(9):
            print(solution['%d-%d' % (row, col)][0], end=' ')
            if col == 2 or col == 5:
                print('|', end=' '),
        print("")
        if row == 2 or row == 5:
            print('------+-------+------')


def solve_sodoku_file(path: str = "easy.txt") -> None:
    csp = create_sudoku_csp(path)
    solution = csp.backtracking_search()
    print_sudoku_solution(solution[0])
    print("Backtracks: ", solution[1])
    print("Failures: ", solution[2])


if __name__ == "__main__":
    # print(timeit.timeit(solve_sodoku_file, number=10)/10)
    solve_sodoku_file("easy.txt")
    solve_sodoku_file("medium.txt")
    solve_sodoku_file("hard.txt")
    solve_sodoku_file("veryhard.txt")
    # csp = create_map_coloring_csp()
    # print(csp.backtracking_search())
