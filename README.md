# Purpose
This project was an assignment in an intro course to artificial intelligence.
We were handed the Assignment.py file, and some sudoku boards, and
had to create a sudoku solver by using backtracking.
The methods we had to implement were `backtrack`, `select_unassigned_variable`,
`inference`, and `revise`.

The project should be runnable without installing any external libraries.
Just run `python3 Assignment.py`. It will spit out solutions to the sudoku boards 
along with the number of times it had to backtrack and the number of times it 
reached an unsolvable state.